# How to contribute

Join our telegram to get started: https://t.me/joinchat/DM8-zRAVi-tEx-1PEltAIg

Check out what we are working on: https://taiga.getmonero.org/project/xmrhaelan-monero-public-relations/epic/26

Github org (request to be added to generate awareness): https://github.com/monero-outreach